// pageObjects/login_page.js

export class LoginPageObjects {

  // emailField: any;
  // passwordField: any;
  // submitButton: any;

    constructor(page) {
      this.emailField = page.locator('[id="email"]');
      this.passwordField = page.locator('input[name="Password"]');
      this.submitButton = page.locator('button[type="submit"]');
    }
  
    getEmailidInputFieldAsText() {
      return this.emailField;

    }
  
    getPasswordInputFieldAsText() {
      return this.passwordField;
    }
    
    getSubmitButton() {
        return this.submitButton;
      }
  }
  