// loadtests/login_test.js

import { browser } from 'k6/experimental/browser';
import { LoginUtils } from '../libs/login.lib.js';

export const options = {
  scenarios: {
    ui: {
      executor: 'constant-vus',
      vus: 1,
      duration: '50s',
      options: {
        browser: {
          type: 'chromium',
        },
      },
    },
  },
  thresholds: {
    checks: ['rate==1.0'],
  },
};


export default async function () {
  const page = browser.newPage();
  try {

    const loginpage = new LoginUtils(page);

    await loginpage.Login()

  } finally {
    // page.close()
  }
}



// export default async function () {
//   const page = browser.newPage();
//   try {
//     const loginpage = new LoginUtils(page);

//     // Iterate through each user in the list
//     for (const userData of loginData.userList) {
//       await loginpage.Login(userData);
//     }
//   } finally {
//     page.close();
//   }
// }

// export default async function () {
//   const page = browser.newPage();
//   try {
//     const loginpage = new LoginUtils(page);
//     await loginpage.loginAllUsers();
//   } finally {
//     page.close();
//   }
// }


// 