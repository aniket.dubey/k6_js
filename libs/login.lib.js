// libs/login.lib.js

import { check } from 'k6';
import { LoginPageObjects } from '../pageObjects/login.page.js';
import { loginPageURL } from '../support/utility.js';
import { loginData } from '../fixtures/login_data.js';
import { SharedArray } from 'k6/data';

const data = new SharedArray('some data name', function () {
  return JSON.parse(open('../fixtures/login_data.json')).userList;
});

export class LoginUtils {

  constructor(page) {
    this.page = page;
    this.loginPageObjects = new LoginPageObjects(page);

  }

  async Login() {

    for (const userData of data) {
      const { user, password } = userData;

      console.log(user)
      console.log(password)

      try {

        await this.page.goto(loginPageURL, { waitUntil: 'networkidle' });

        this.loginPageObjects.getEmailidInputFieldAsText().type(user);
        this.loginPageObjects.getPasswordInputFieldAsText().type(password);
        await Promise.all([this.page.waitForNavigation(), this.loginPageObjects.getSubmitButton().click()]);

        check(this.page, {
          'Verify text': () =>
            this.page.locator('.infoText').textContent() === 'View and manage your organizations',
        });


      } finally {
        this.page.close()
      }


    }
  }

}




// async Login() {
//   await this.page.goto(loginPageURL, { waitUntil: 'networkidle' });

//   // const { user, password } = loginData

//   // this.loginPageObjects.getEmailidInputFieldAsText().type(user);
//   // this.loginPageObjects.getPasswordInputFieldAsText().type(password);

//   const user = data[0].user
//   const password = data[0].password

//   this.loginPageObjects.getEmailidInputFieldAsText().type(user);
//   this.loginPageObjects.getPasswordInputFieldAsText().type(password);

//   await Promise.all([this.page.waitForNavigation(), this.loginPageObjects.getSubmitButton().click()]);

//   check(this.page, {
//     'Verify text': () =>
//       this.page.locator('.infoText').textContent() === 'View and manage your organizations',
//   });
// }

